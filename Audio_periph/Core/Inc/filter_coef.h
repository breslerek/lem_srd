/*
 * filter_coef.h
 *
 *  Created on: Dec 29, 2020
 *      Author: bresl
 */

#ifndef INC_FILTER_COEF_H_
#define INC_FILTER_COEF_H_

#define l_filter_size 101
#define e_filter_size 1186
#define m_filter_size 730
#define o_filter_size 801
#define n_filter_size 711

extern const float l_filter_coeff[101];
extern const float e_filter_coeff[1186];
extern const float m_filter_coeff[730];
extern const float o_filter_coeff[801];
extern const float n_filter_coeff[711];

#endif /* INC_FILTER_COEF_H_ */
