load('l_filtr'); load('e_filtr'); load('m_filtr'); load('o_filtr'); load('n_filtr');

%%
l_file = fopen('l_data.txt', 'w');
for i=1:length(l_filtr)
    fprintf(l_file,'%1.6f, ', l_filtr(i));
end
fclose(l_file);

%%
e_file = fopen('e_data.txt', 'w');
for i=1:length(e_filtr)
    fprintf(e_file,'%1.6f, ', e_filtr(i));
end
fclose(e_file);

%%
m_file = fopen('m_data.txt', 'w');
for i=1:length(m_filtr)
    fprintf(m_file,'%1.6f, ', m_filtr(i));
end
fclose(m_file);

%%
o_file = fopen('o_data.txt', 'w');
for i=1:length(o_filtr)
    fprintf(o_file,'%1.6f, ', o_filtr(i));
end
fclose(o_file);

%%
n_file = fopen('n_data.txt', 'w');
for i=1:length(n_filtr)
    fprintf(n_file,'%1.6f, ', n_filtr(i));
end
fclose(n_file);

