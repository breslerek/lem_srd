function y = get_sound(time)
fs = 44100;
for i=1:5
    word = get_audio(fs, time, i);
    pause(1)
    tmp = cisza(word,fs,0.15);
    tmp(end:40000) = 0;
    tmp_audio(:,i) = tmp;
end
sound_word = sum(tmp_audio,2)/5;
sound_word = cisza(sound_word,fs);
sound_filtr = sound_word - mean(sound_word);
y = sound_filtr(end:-1:1);
end
