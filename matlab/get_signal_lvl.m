function y = get_signal_lvl(fs, rec_num, rec_length, word_length, filter_coeff)

word_init = zeros(1,rec_num);
word_lvl = zeros(1,rec_num);
if_word = zeros(rec_length*fs, rec_num);
for i=1:rec_num
    pause(1)
    tmp = get_audio(fs, rec_length);
    if_word(:,i) = filter(filter_coeff, 1, tmp);
    prompt = 'Second at which word was said: ';
    word_init(i) = input(prompt);
    word_start = round(word_init(i) * fs);
    word_end = round((word_init(i)+word_length)*fs);
    peak = max(if_word(word_start:word_end,i));
    check_1 = max(if_word(1:word_start,i));
    check_2 = max(if_word(word_end:end,i));
    if check_1 > peak || check_2 >  peak 
        disp('Recording not done properly')
        break
    end
    word_lvl(i) = peak;
end

y = sum(word_lvl)/rec_num;

end
