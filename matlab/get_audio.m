function y = get_audio(fs, time, fig_num)

recObj = audiorecorder(fs,16,1,-1);
disp('Start speaking.');
recordblocking(recObj, time);
disp('End of Recording.');
y = getaudiodata(recObj);
if nargin == 3
    figure(fig_num)
    plot(y);
    xlabel('Probka')
    ylabel('Amplituda')
end

end
