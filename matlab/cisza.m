function y = cisza(x,fs,prog_value)

dt1 = 0.01; Mlen = floor(dt1*fs);
dt2 = 0.001; Mstep = floor(dt2*fs);

if nargin == 2
    prog = 0.25*fs/8000;
elseif nargin == 3
    prog = prog_value*fs/8000;
end
N = length(x);
Nramek = floor((N-Mlen)/Mstep+1);
xn = x/max(abs(x));
for nr1 = 1:Nramek
   bx = xn(1+(nr1-1)*Mstep:Mlen+(nr1-1)*Mstep);
   if(bx'*bx>=prog)
       break
   end
end

for nr2=Nramek:-1:nr1
   bx = xn(1+(nr2-1)*Mstep:Mlen+(nr2-1)*Mstep);
   if(bx'*bx>=prog)
       break
   end
end    
y = x(1+(nr1-1)*Mstep:Mlen+(nr2-1)*Mstep);
end
