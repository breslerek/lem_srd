function [ctrl, fstate] = if_found(data, filtr_1, filtr_2, prev, letter_lvl)

if max(data) > letter_lvl
     ctrl = prev + 1;
     fstate = zeros([length(filtr_2)-1, 1]).';
else 
    ctrl = prev;
    fstate = zeros([length(filtr_1)-1, 1]).';
end

end