%% Get filter coefficients for "l" letter

clear all; close all; 
l_filtr_raw = get_sound(2);
l_filtr = cut_filter(l_filtr_raw);
% save('l_filtr','l_filtr')

%% Get filter level for "l" letter
l_lvl = get_signal_lvl(44100, 1, 5, 1.5, l_filtr);

%% Get filter coefficients for "e" letter

clear all; close all; 
e_filtr_raw = get_sound(2);
e_filtr = cut_filter(e_filtr_raw);
% save('e_filtr','e_filtr')

%% Get filter level for "e" letter
e_lvl = get_signal_lvl(44100, 1, 5, 1.5, e_filtr);

%% Get filter coefficients for "m" letter

clear all; close all; 
m_filtr_raw = get_sound(2);
m_filtr = cut_filter(m_filtr_raw);
% save('m_filtr','m_filtr')

%% Get filter level for "m" letter
m_lvl = get_signal_lvl(44100, 1, 5, 1.5, m_filtr);

%% Get filter coefficients for "n" letter

clear all; close all; 
n_filtr_raw = get_sound(2);
n_filtr = cut_filter(n_filtr_raw);
% save('n_filtr','n_filtr')

%% Get filter level for "n" letter
n_lvl = get_signal_lvl(44100, 1, 5, 1, n_filtr);

%% Get filter coefficients for "o" letter

clear all; close all; 
o_filtr_raw = get_sound(2);
o_filtr = cut_filter(o_filtr_raw);
% save('o_filtr','o_filtr')

%% Get filter level for "o" letter
o_lvl = get_signal_lvl(44100, 1, 5, 1, o_filtr);