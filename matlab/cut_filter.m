function y = cut_filter(filter_data)
r = xcorr(filter_data, filter_data);
plot(r)
disp('Analyze the plot. Choose two x values corresponding to peaks in chart, that representu full period')
prompt = 'Value for x1:\n';
x1= input(prompt);
prompt = 'Value for x2:\n';
x2= input(prompt);
y = filter_data(x1:x2);
end
