close all; close all;

load('l_filtr'); load('e_filtr'); load('m_filtr'); load('o_filtr'); load('n_filtr');
load('letter_levels');
fs = 44100;
ctrl = 0;
block = 2048;
result = [];
blocks = [];
filter_coeff = l_filtr;
fstate = zeros([length(l_filtr)-1, 1]).';
nagranie = get_audio(fs, 20, 1);
lem_ok = 0;
lem_start = 0;
on_off = 0;
liczba_ramek = floor(length(nagranie)/block);

for i=1:liczba_ramek
    blocks(i,:) = nagranie(block*(i-1)+1:block*i);
end

for i=1:liczba_ramek
    
    data_to_filt = [fstate blocks(i,:)];
    data = filter(filter_coeff, 1, data_to_filt);
    
    switch(ctrl)        
        case 0
            [ctrl, fstate] = if_found(data, l_filtr, e_filtr, ctrl, l_lvl);
            if ctrl == 1
                if i < 22
                    filter_coeff = e_filtr;
                    start_block = i;
                    lem_start = i;
                elseif i - lem_start > 44
                    filter_coeff = e_filtr;
                    start_block = i;
                    lem_start = i;
                else 
                    ctrl = 0;
                    filter_coeff = l_filtr;
                    fstate = zeros([length(l_filtr)-1, 1]).';                    
                end
            end        
        case 1
            [ctrl, fstate] = if_found(data, e_filtr, m_filtr, ctrl, e_lvl);
            if ctrl == 2
                filter_coeff = m_filtr;
                start_block = i;
            end
            if i - start_block > 9 
                ctrl = 0;
                filter_coeff = l_filtr;
                fstate = zeros([length(l_filtr)-1, 1]).';               
            end        
        case 2
            [ctrl, fstate] = if_found(data, m_filtr, o_filtr, ctrl, m_lvl);
            if ctrl == 3
                filter_coeff = o_filtr;
                start_block = i;
                disp('Wypowiedziano LEM')
                disp(i);
            end
            if i - start_block > 9 
                ctrl = 0;
                filter_coeff = l_filtr;
                fstate = zeros([length(l_filtr)-1, 1]).';
            end        
        case 3
            [ctrl, fstate] = if_found(data, o_filtr, n_filtr, ctrl, o_lvl);
            if ctrl == 4
                filter_coeff = n_filtr;
                start_block = i;
            end
            if i - start_block > 22 
                ctrl = 0;
                filter_coeff = l_filtr;
                fstate = zeros([length(l_filtr)-1, 1]).';
            end   
        case 4
            [ctrl, fstate] = if_found(data, n_filtr, l_filtr, ctrl, n_lvl);
            if ctrl == 5
                filter_coeff = l_filtr;
                fstate = zeros([length(l_filtr)-1, 1]).';
                ctrl = 0;
                disp('Device turned ON')
            end    
            if i - start_block > 9 
                ctrl = 0;
                filter_coeff = l_filtr;
                fstate = zeros([length(l_filtr)-1, 1]).';
                disp('Device turned OFF')
            end    
    end
    
    fstate = data(length(data) - length(fstate):end);
    result = [result data];
    
end

plot(result)

